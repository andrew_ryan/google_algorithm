#[allow(warnings)]
pub mod problems {
    pub struct Solution {}
    /// leetcode 1
    /// https://leetcode.cn/problems/largest-triangle-area/
    impl Solution {
        pub fn largest_triangle_area(list: Vec<Vec<i32>>) -> f64 {
            /// Since there are only 50 points at most, we can directly enumerate all triangles.
            let mut max = 0.0;
            list.iter().for_each(|x| {
                list.iter().for_each(|y| {
                    list.iter().for_each(|z| {
                        let a = f64::abs(
                            (x[0] * (y[1] - z[1]) + y[0] * (z[1] - x[1]) + z[0] * (x[1] - y[1]))
                                as f64,
                        ) * 0.5;
                        if a > max {
                            max = a;
                        }
                    });
                });
            });
            max
        }
    }
    /// leetcode 2
    /// https://leetcode.com/problems/two-sum/
    impl Solution {
        pub fn two_sum(nums: Vec<i32>, taget: i32) -> Vec<i32> {
            let mut map = std::collections::BTreeMap::new();
            for i in 0..nums.len() {
                if let Some(&j) = map.get(&(taget - nums[i])) {
                    return vec![j as i32, i as i32];
                } else {
                    map.insert(nums[i], i);
                }
            }
            vec![]
        }
    }
    /// leetcode 3
    /// https://leetcode.com/problems/add-two-numbers/
    /// Definition for singly-linked list.
    #[derive(PartialEq, Eq, Clone, Debug)]
    pub struct ListNode {
        pub val: i32,
        pub next: Option<Box<ListNode>>,
    }

    impl ListNode {
        #[inline]
        fn new(val: i32) -> Self {
            Self { next: None, val }
        }
    }
    impl Solution {
        pub fn add_two_numbers(
            l1: Option<Box<ListNode>>,
            l2: Option<Box<ListNode>>,
        ) -> Option<Box<ListNode>> {
            Solution::add(l1, l2, 0)
        }

        fn add(
            l1: Option<Box<ListNode>>,
            l2: Option<Box<ListNode>>,
            carry: i32,
        ) -> Option<Box<ListNode>> {
            if l1.is_none() && l2.is_none() && carry == 0 {
                return None;
            }
            let (a, n1) = match l1 {
                Some(n) => (n.val, n.next),
                None => (0, None),
            };
            let (b, n2) = match l2 {
                Some(n) => (n.val, n.next),
                None => (0, None),
            };

            let x = a + b + carry;

            let mut node = ListNode::new(x % 10);
            node.next = Solution::add(n1, n2, x / 10);
            Some(Box::new(node))
        }
    }

    #[cfg(test)]
    pub mod test {
        use super::*;
        #[test]
        fn largest_triangle_area_test() {
            assert_eq!(
                Solution::largest_triangle_area(vec![vec![4, 6], vec![6, 5], vec![3, 1]]),
                5.5
            );
            assert_eq!(
                Solution::largest_triangle_area(
                    [
                        [0, 0].to_vec(),
                        [0, 1].to_vec(),
                        [1, 0].to_vec(),
                        [0, 2].to_vec(),
                        [2, 0].to_vec(),
                    ]
                    .to_vec(),
                ),
                2.0
            );
        }
        #[test]
        fn two_sum_test() {
            assert_eq!(
                Solution::two_sum([2, 7, 11, 15].to_vec(), 9),
                [0, 1].to_vec()
            );
        }
        #[test]
        fn add_two_numbers_test() {}
    }
    pub fn run() {
        assert_eq!(
            Solution::largest_triangle_area(vec![vec![4, 6], vec![6, 5], vec![3, 1]]),
            5.5
        );
        let mut l1 = Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode {
                val: 3,
                next: Some(Box::new(ListNode {
                    val: 4,
                    next: Some(Box::new(ListNode { val: 5, next: None })),
                })),
            })),
        }));
        let mut l2 = Some(Box::new(ListNode {
            val: 2,
            next: Some(Box::new(ListNode {
                val: 4,
                next: Some(Box::new(ListNode {
                    val: 5,
                    next: Some(Box::new(ListNode { val: 6, next: None })),
                })),
            })),
        }));

        let result = Solution::add_two_numbers(l1.clone(), l2.clone());
        println!("{:?}", l1);
        println!("{:?}", l2);
        println!("{:?}", result);
    }
}
